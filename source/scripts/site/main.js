$(window).on('load', function () {
    $('body').addClass('is-loaded'); 
});

function animateFrom(elem, direction) {
    direction = direction || 1;
    var x = 0,
        y = -(direction * 100);
    if(elem.classList.contains("gs_reveal_fromLeft")){
        x = -200;
        y = 0;
    } else if(elem.classList.contains("gs_reveal_fromRight")){
        x = 200;
        y = 0;
    }
    elem.style.transform = "translate(" + x + "px, " + y + "px)";
    elem.style.opacity= 0;
    gsap.fromTo(elem, {x:x,y:y, autoAlpha:0}, {
        duration: 2.5,
        x: 0,
        y: 0,
        autoAlpha: 1,
        ease: "expo",
        overwrite: "auto"
    })
}
function hide(elem) {
    gsap.set(elem, {autoAlpha:0})
}
document.addEventListener("DOMContentLoaded", function() {
    gsap.registerPlugin(ScrollTrigger);
    gsap.utils.toArray(".gs_reveal").forEach(function(elem){
        hide(elem);
        ScrollTrigger.create({
            trigger: elem,
            onEnter: function() {animateFrom(elem)},
            onEnterBack: function() {animateFrom(elem, -1)},
            onLeave: function() {hide(elem)}
        })
    })
})

//image comparison
gsap.registerPlugin(ScrollTrigger);
gsap.utils.toArray(".comparison").forEach(section => {
    let tl = gsap.timeline({
        scrollTrigger: {
            trigger: section,
            start: "center center",
            end: ()=> "+=" + section.offsetWidth,
            scrub: true,
            pin: true,
            anticipatePin: 1,
            
        },
        defaults: {ease: "none"}
    });
    tl.fromTo(section.querySelector(".comparison__image--after"), {
        xPercent: 100, x:0}, {xPercent: 0})
      .fromTo(section.querySelector(".comparison__image--after img"),
      {xPercent:-100,x:0}, {xPercent:0}, 0);
});

//coffee drink
var frameCount = 9,
    offsetValue = 100;

gsap.to(".viewer", {
    backgroundPosition: (-offsetValue * frameCount *2) + "px 50%",
    ease: "steps(" + frameCount + ")",
    scrollTrigger: {
        trigger: ".coffee",
        start: "center top",
        markers: false,
        end: "+=" + (frameCount * offsetValue),
        pin: true,
        scrub: true
    }
})

//scroll image
const delayedSec = document.querySelectorAll(".delayed-section");
delayedSec.forEach(section => {
    const containerAnim = gsap.to(section.querySelector(".image__container"), {
        y: "100vh",
        ease: "none"
    });

    const imageAnim = gsap.to(section.querySelector(".image__container img"), {
        y: "-100vh",
        ease: "none",
        paused: true
    });

    const scrub = gsap.to(imageAnim, {
        progress: 1,
        paused: true,
        ease: "power3",
        duration: parseFloat(section.dataset.scrub) || 0.1,
        overwrite: true
    });

    ScrollTrigger.create({
        animation: containerAnim,
        scrub: true,
        trigger: section,
        start: "top bottom",
        end: "bottom top",
        onUpdate: self => {
            scrub.vars.progress = self.progress;
            scrub.invalidate().restart();
          }
    })
})

// //FLIP
// // gsap.registerPlugin(Flip);
// // let layouts = ["for","plain","columns","layout"],
// //     container = document.querySelector(".wrg.flip"),
// //     currentLayout=0;

// // function nextState() {
// //     const state = Flip.getState(".flip__letter,.flip__label--for,.flip__label--gsap",
// //     {props: "color,backrgroundColor", simple:true});
// //     //container.classList.remove(layouts[currentLayout]);
// //     currentLayout = (currentLayout + 1) % layouts.length;
// //     container.classList.add(layouts[currentLayout]);

// //     Flip.from(state, {
// //         absolute: true,
// //         stagger: 0.07,
// //         duration: 0.7,
// //         ease: "power2.inOut",
// //         spin: currentLayout === 0,
// //         simple: true,
// //         onEnter: (elements,animation) => gsap.fromTo(elements, {opacity:0}, {opacity:1, delay:animation.duration() - 0.1}),
// //         onLeave: elements => gsap.to(elements, {opacity: 0})
// //     });

// //     gsap.delayedCall(currentLayout === 0 ? 3.5 : 1.5. nextState);
// // }
// // gsap.delayedCall(1,nextState);

//Gsap To

let tlTo = gsap.timeline(
    {defaults:{duration: 1,translateY:"-50%",opacity:1},
    scrollTrigger: {
        trigger: '.gsap--to',
        start: "bottom center",
        toggleAction: "restart restart restart pause",
        pin:true
    }}
);
var gsapLogo1 = document.querySelector(".gsap__logo--left");
var gsapLogo2 = document.querySelector(".gsap__logo--right");
var gsapText = document.querySelector(".gsap__text");
tlTo.to(gsapLogo1, {top:"50%",left:"30%", translateX:"-50%"})
tlTo.to(gsapLogo2, {top:"50%",right:"20%", translateX:"-50%"}, '-=1')
tlTo.to(gsapText, {opacity:1, bottom:"45%", scale:1,translateX:"-50%"}, '-=1')

//Gsap From
let tlFrom = gsap.timeline(
    {defaults:{duration: .5,opacity:0},
    scrollTrigger: {
        trigger: '.gsap--from',
        start: "bottom bottom",
        toggleAction: "restart restart restart restart",
        scrub: 1,
        pin:true,
    }}
);
var gsapLogo1 = document.querySelector(".gsap-from .gsap__logo--left");
var gsapLogo2 = document.querySelector(".gsap-from .gsap__logo--right");
var gsapText = document.querySelector(".gsap-from .gsap__text");
tlFrom.from(gsapLogo1, {top:"50%",left:"-50%", translateX:"-50%"})
tlFrom.from(gsapLogo2, {top:"50%",right:"-50%", translateX:"-50%"}, '-=.5')
tlFrom.from(gsapText, {bottom:"-45%", scale:0,translateX:"-50%"}, '-=.5')

//Gsap FromTo
let tlFromTo = gsap.timeline(
    {defaults:{duration: 0.5},
    scrollTrigger: {
        trigger: '.gsap--from-to',
        start: "bottom bottom",
        toggleAction: "restart restart restart restart",
        scrub: 1,
        pin:true,
    }}
);
var gsapLogo1 = document.querySelector(".gsap-from-to .gsap__logo--left");
var gsapLogo2 = document.querySelector(".gsap-from-to .gsap__logo--right");
var gsapText = document.querySelector(".gsap-from-to .gsap__text");
tlFromTo.fromTo(gsapLogo1,
    {left:"-50%",top:"50%", translate:"-50% -50%",opacity:0},
    {left:"25%",opacity:1});
tlFromTo.fromTo(gsapLogo2,
    {right:"-50%",top:"50%", translate:"-50% -50%",opacity:0},
    {right:"25%",opacity:1}, '-=.5');
tlFromTo.fromTo(gsapText, 
    {left:"-25%",bottom:"-45%", scale:0,opacity:0},
    {left:"50%",opacity:1, bottom:"45%",translateX:"-50%", translateY:"-50%", scale:1}, '-=.5');




// Text animations

const heading = new SplitText(".text__heading", {type:"words,chars"});
const textBg = document.querySelector('.text__bg');
const headingChars = heading.chars;

gsap.set(headingChars, {
    opacity: 1,
    scaleY:2,
    yPercent: 160,
    transformOrigin: "50% 0%"
});

gsap.to(headingChars, {
    duration: 0.8,
    stagger: .03,
    ease: Back.easeOut.config(2.5),
    yPercent: 0,
    delay:1,
});

gsap.to(headingChars, {
    duration: 0.6,
    stagger: 0.03,
    ease: "power2.out",
    opacity: 1,
    scaleY:1,
    delay:1,
}, "+=0.2");

gsap.from(".text__intro", {
    opacity: 0,
    y: 50,
    duration: 1,
    ease: "power2.out",
    delay:1
});

gsap.to(textBg, {
    yPercent:110,
    duration:2,
    delay:1,
    ease: "power2.out"

})

//boxes

const box1 = document.querySelector('.box--1');
const box2 = document.querySelector('.box--2');
const box3 = document.querySelector('.box--3');
const box4 = document.querySelector('.box--4');

const box_tl = gsap.timeline({ease:'Sine.in'});
gsap.set('.box', {
    autoAlpha:0,
})
gsap.to('.box', {
    stagger: {
        grid: [2,2],
        from: "center",
        amount: 1.5
    }
})
box_tl.to('.box--1',{autoAlpha:1,scale:1,stagger:0.5, left:0, top:0,duration:0.8,delay:.5});
box_tl.to('.box--2',{autoAlpha:1,scale:1,stagger:0.5, left:'50%', top:0,duration:0.8,xPercent:0,yPercent:0});
box_tl.to('.box--3',{autoAlpha:1,scale:1,stagger:0.5, left:0, top:'50%',duration:0.8,xPercent:0,yPercent:0});
box_tl.to('.box--4',{autoAlpha:1,scale:1,stagger:0.5, left:'50%', top:'50%',duration:0.8,xPercent:0,yPercent:0});

// Circles
gsap.registerPlugin(ScrollTrigger);
const circleOne = $('.circle-block--one');
const circleTwo = $('.circle-block--two');
const circleThree = $('.circle-block--three');
const circleOneText = $('.circle-block--one span');
const circleTwoText = $('.circle-block--two span');
const circleThreeText = $('.circle-block--three span');
let circleTl = gsap.timeline({
    scrollTrigger: {
        trigger: '.jsCircles',
        start: "top 30%",
        end: "90%",
        pin: true,
        scrub: 2,
        markers: true,
        toggleActions: "restart pause reverse pause"
    }
})
    circleTl.from(circleOne, {x: "-150%"})
            .from(circleOneText, {x: "300%",opacity:0},'-=.5')
            .from(circleThree, {x: "150%"},'-=.5')
            .from(circleThreeText, {x: "-300%"},'-=.5')
            .from(circleTwo, {y: "150%"},'-=.5')
            .from(circleTwoText, {y: "-300%"},'-=.5')
            




